'use strict';

const mongoose = require('mongoose');

module.exports = {
    processQueue: function (req, res, next, _params, queue) {
        queue.forEach(el => {
            let sourceObj = (_params[el].isParam) ? req.params : req.body;
            _params[el].validate(sourceObj[el], _params)
                .then(() => {
                    var index = queue.indexOf(el);
                    if (index !== -1) queue.splice(index, 1);
                    if (queue.length == 0)
                        return next();
                })
                .catch(err => {
                    return res.json({
                        response: false,
                        error: 'INVALID_FIELD',
                        message: _params[el].errorMessage || `Please input a valid '${el}'`
                    });
                });
        });
    },
    checkParams: function (params) {

        return (req, res, next) => {
            let _params = {};
            let _onQueue = [];

            for (var i = 0; i < params.length; i++) {
                /**
                 * i changes every loop, _i is local to every loop therefore
                 * is not changed in every loop, instead it is re-declared
                 * containing the current loop's index
                 */
                let _i = i;
                let name = null;
                let pseudoName = null;
                let param = params[i];
                let sourceObj = (param.isParam) ? req.params : req.body;
                
                // If the param can have multiple names
                if (Object.prototype.toString.call(param.name) == '[object Array]') {
                    for (var j = 0; j < param.name.length; j++)
                        if (param.name[j] in sourceObj) {
                            name = pseudoName = param.name[j];
                            break;
                        }
                    if (name == null) pseudoName = param.name.join(' or ');
                }
                // If it has a single name
                else {
                    if (param.name in sourceObj)
                        name = pseudoName = param.name;
                }

                // If the param is not present
                if (name == null) {
                    return res.json({
                        response: false,
                        error: 'MISSING_FIELDS',
                        message: `${pseudoName || param.name} is required.`
                    });
                }

                // Call the validate function and pass the validated object
                param.validate(sourceObj[name], _params)
                    .then(() => {
                        _params[name] = sourceObj[name];
                        // If everything was validated and nothing is on queue call next
                        if (Object.keys(_params).length == params.length && _onQueue.length == 0)
                            return next();
                        // If everything was validated and there are things on queue call process queue
                        else if (Object.keys(_params).length == params.length && _onQueue.length > 0)
                            return this.processQueue(req, res, next, _params, _onQueue);
                    })
                    .catch((err = {}) => {
                        if (err.dependsOnParam) {
                            _onQueue.push(param.name);
                            _params[param.name] = param;
                            if (Object.keys(_params).length == params.length)
                                return this.processQueue(req, res, next, _params, _onQueue);
                        } else {
                            // console.log(err);
                            return res.json({
                                response: false,
                                error: 'INVALID_FIELD',
                                message: param.errorMessage || err.message || `Please input a valid '${pseudoName}'`
                            });
                        }
                    });

            }
            // console.log('HERE');
        }
    },
    // Compatibility
    checkReqParams: function (params) {
        return this.checkParams(params);
    },
    validate: {
        checkEmpty: function (value) {
            if (typeof value == 'string') {
                return /\S/.test(value);
            } else if (Object.prototype.toString.call(value) === '[object Array]') {
                return value.length > 0;
            }
    
            return false;
        },
        checkBase64: function (value) {
            return /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/.test(value);
        },
        notEmpty: function (value, obj) {
            return new Promise((resolve, reject) => {
                if (typeof value == 'string') {
                    if (/\S/.test(value)) return resolve();
                } else if (Object.prototype.toString.call(value) === '[object Array]') {
                    if (value.length > 0) return resolve();
                }
                return reject();
            });
        },
        mongooseId: function (value, obj) {
            return new Promise((resolve, reject) => {
                if (!(typeof value == 'string'))
                    return reject();
                if (mongoose.Types.ObjectId.isValid(value))
                    return resolve();
                    
                return reject();
            });
        }
    }
};

// let required = [
//     {
//         name: 'password',
//         validate: function (val, obj) {
//             return new Promise((resolve, reject) => {
//                 return resolve();
//             });
//         }
//     },
//     {
//         name: 'confirmPassword',
//         dependsOn: 'password',
//         errorMessage: 'Passwords don\'t match',
//         validate: function (val, obj) {
//             return new Promise((resolve, reject) => {
//                 if ('password' in obj) {
//                     if (val == obj.password) {
//                         return resolve();
//                     } else {
//                         return reject({});
//                     }
//                 } else {
//                     return reject({
//                         dependsOnParam: true
//                     });
//                 }
//             });
//         }
//     },
//     {
//         name: 'name',
//         validate: a.validate.notEmpty
//     }
// ];

// a.checkParams(required)({body: {password: '123', confirmPassword: '1223', name: ''}}, {json: function(_a) {console.log(_a)}}, function () {console.log('Called next')});